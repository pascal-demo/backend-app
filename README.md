# Usage

Building the image locally:

Running the container locally requires the `username:password` for the database
and the `SECRET_KEY` used for caching.

```
docker build -t hello-world-app:latest .                                                                                                                                                                                                            
docker run -it -p 5000:5000 --name hello -e DATABASE_URL="postgresql://db_admin:xxxxxxxxxx@aurora-demo-cluster.cluster-yyy.us-east-1.rds.amazonaws.com/postgres" -e SECRET_KEY="xxxxx" hello-world-app:latest
```
