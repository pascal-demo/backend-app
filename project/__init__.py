from flask import Flask, jsonify, request, flash
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS


app = Flask(__name__)
CORS(app)
app.config.from_object("project.config.Config")
app.config['TRAP_BAD_REQUEST_ERRORS'] = True
db = SQLAlchemy(app)
socketio = SocketIO(app, cors_allowed_origins='*')

class clicks(db.Model):
    __tablename__ = "click_monitor"

    id = db.Column(db.Integer, primary_key=True)
    count = db.Column(db.Integer(), nullable=False)

    def __init__(self, count):
        self.count = count

@app.route("/")
def hello_world():
    return jsonify(hello="world")

@app.route('/count')
def get_count():
   click = clicks.query.first()
   return jsonify(count=click.count)

@app.route('/click', methods = ['OPTIONS', 'POST'])
def update():
   if request.method == 'POST':
         click = clicks.query.first()
         click.count += 1
         db.session.commit()

         success = jsonify(success=True)
         flash('Record was successfully updated')
         socketio.emit('clickEmit', {'count':click.count}, broadcast=True)
         return success
   elif request.method == 'OPTIONS':
      return jsonify(success=False)
   else:
      return 'bad request', 400

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', debug=True)
